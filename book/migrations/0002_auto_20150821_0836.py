# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('book', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bookpart',
            name='book',
            field=models.ForeignKey(verbose_name=b'name', to='book.Book'),
        ),
        migrations.AlterField(
            model_name='text',
            name='book_part',
            field=models.ForeignKey(verbose_name=b'title', to='book.BookPart'),
        ),
    ]
