# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('book', '0002_auto_20150821_0836'),
    ]

    operations = [
        migrations.AlterField(
            model_name='book',
            name='name',
            field=models.CharField(max_length=100, verbose_name=b'name'),
        ),
        migrations.AlterField(
            model_name='bookpart',
            name='book',
            field=models.ForeignKey(to='book.Book'),
        ),
        migrations.AlterField(
            model_name='bookpart',
            name='title',
            field=models.CharField(max_length=200, verbose_name=b'title'),
        ),
        migrations.AlterField(
            model_name='text',
            name='book_part',
            field=models.ForeignKey(to='book.BookPart'),
        ),
    ]
