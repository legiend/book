from django import forms


class SearchForm(forms.Form):
    email = forms.EmailField(
        widget=forms.TextInput(attrs={'class': 'form-control'}))
    text = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}))
    time = forms.FloatField(min_value=0, required=False)
