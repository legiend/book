# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('book', '0004_auto_20150821_0843'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bookpart',
            name='book',
            field=models.ForeignKey(to='book.Book'),
        ),
        migrations.AlterField(
            model_name='text',
            name='book_part',
            field=models.ForeignKey(to='book.BookPart'),
        ),
    ]
