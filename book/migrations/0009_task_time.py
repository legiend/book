# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('book', '0008_auto_20150825_0652'),
    ]

    operations = [
        migrations.AddField(
            model_name='task',
            name='time',
            field=models.IntegerField(default=0),
        ),
    ]
