# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def load_stores_from_sql():
    from book_search.settings import BASE_DIR
    import os
    sql_statements = open(os.path.join(
    	BASE_DIR,
    	'book/sql/search_to_temp.sql'), 
    'r').read()
    return sql_statements


class Migration(migrations.Migration):

    dependencies = [
        ('book', '0010_auto_20150825_0727'),
    ]

    operations = [
    	migrations.RunSQL(
    		load_stores_from_sql(), 
    		'DROP FUNCTION get_to_virtual(character varying, '\
                'character varying, character varying);'
    	),
    ]
