CREATE OR REPLACE FUNCTION GET_TO_VIRTUAL(search varchar(200), table_name varchar(150), time_limit varchar(20)) RETURNS void AS $$
    DECLARE
        item record;
        create_table varchar(300) = '
        CREATE TEMPORARY TABLE %s(
            id   serial PRIMARY KEY,
            name varchar(255),
            title varchar(20000),
            page integer
        );';
        insert_to varchar(600) = '
            INSERT INTO %s(name, title, page) VALUES (%L, %L, %L)
        ';

    BEGIN

        EXECUTE format(create_table, table_name);
        FOR item IN
        (
            SELECT book_book.name, book_bookpart.title, book_text.page
            FROM book_text JOIN book_bookpart ON 
            book_text.book_part_id=book_bookpart.id JOIN 
            book_book ON book_bookpart.book_id=book_book.id
            WHERE book_text.text LIKE CONCAT('%', search, '%')
        )
        LOOP
            EXECUTE format(insert_to, table_name, item.name, item.title, item.page);
           
            IF clock_timestamp() - now() > time_limit::INTERVAL THEN
        RETURN;
        END IF;
        END LOOP;
    END;
    $$ LANGUAGE plpgsql;