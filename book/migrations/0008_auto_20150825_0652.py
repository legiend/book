# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('book', '0007_auto_20150825_0609'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='bookpart',
            name='page',
        ),
        migrations.AddField(
            model_name='text',
            name='page',
            field=models.IntegerField(default=0),
        ),
    ]
