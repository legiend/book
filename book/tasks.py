import datetime
import logging

from celery import task

from book.actions import search_text
from book.mails import send_result


logger = logging.getLogger(__name__)


@task()
def search_text_in_database(data):
    """
    Searching term in database
    """
    start_time = datetime.datetime.now()
    result = search_text(data)
    diff = datetime.datetime.now() - start_time
    logger.info("Time of search: {time} by term \"{term}\"".format(
        time=diff, term=data.search_words))

    send_mail.delay(data=result, user_data=data)


@task()
def send_mail(data, user_data):
    """
    Here we send email with results
    """
    send_result(data, user_data)
