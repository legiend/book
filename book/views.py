from django.shortcuts import render_to_response
from django.views.generic.edit import FormView
from django.views.generic import TemplateView
from django.shortcuts import redirect
from django.core.urlresolvers import reverse

from book.forms import SearchForm
from book.models import Task


class SearchPage(FormView):
    """
    Page with search form
    """

    template_name = 'book/index2.html'
    form_class = SearchForm
    success_url = '/result/'

    def get_success_url(self):
        pk = self.kwargs.get('task_id')
        return reverse('form-result', kwargs={'pk': pk})

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.
        #search_text_in_database.delay(data=form.data)

        self.kwargs["task_id"] = Task.objects.create(
            email=form.data.get("email"),
            search_words=form.data.get("text"),
            time=form.data.get("time") or 0,
        ).pk
        
        return super(SearchPage, self).form_valid(form)


class ResultView(TemplateView):
    """
    Waiting page.
    """

    template_name = "book/result.html"

    def get(self, request, *args, **kwargs):
        email = Task.objects.get(pk=kwargs.get("pk")).email
        return render_to_response(
            self.template_name,
            {'email': email}
        )
