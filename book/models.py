from django.db import models


class Book(models.Model):
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return self.name


class BookPart(models.Model):
    book = models.ForeignKey(Book)
    title = models.CharField(max_length=200)

    def __unicode__(self):
        return self.book.name + ": " + self.title


class Text(models.Model):
    book_part = models.ForeignKey(BookPart)
    text = models.TextField()
    page = models.IntegerField(default=0)


class Task(models.Model):
    search_words = models.CharField(max_length=255)
    email = models.EmailField(max_length=254)
    time = models.FloatField(default=0)


from .signals import *
