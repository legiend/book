from django.db.models.signals import post_save
from django.dispatch import receiver

from book.models import Task
from book.tasks import search_text_in_database


@receiver(post_save, sender=Task)
def start_search(sender, instance, created, raw, *args, **kwargs):
    # Using the statuses from the above method to create a PartLog entry.
    search_text_in_database.delay(data=instance)
