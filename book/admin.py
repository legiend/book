from django.contrib import admin
from models import Book, BookPart, Text, Task


# Register your models here.
admin.site.register(Book)
admin.site.register(BookPart)
admin.site.register(Text)
admin.site.register(Task)
