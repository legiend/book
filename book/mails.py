from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string


def send_result(data, user_data):
    """
    This function send email using django EmailMultiAlternatives
    """
    subject = 'By term \"{term}\" we found {num} results'.format(
        term=user_data.search_words, num=len(data))
    to = user_data.email
    from_email = 'booking.searching@gmail.com <booking.searching@gmail.com>'
    text_content = 'This is an important message.'
    html_content = render_to_string('book/mail.html', {"data": data})

    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
    msg.attach_alternative(html_content, "text/html")
    msg.send()
