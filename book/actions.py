import re
import datetime
import hashlib

from models import Book, BookPart, Text
from django.db import connection


def search_text(data):
    cursor = connection.cursor()
    table_name = "a" + hashlib.sha224(data.email+data.search_words).hexdigest()

    proc = cursor.callproc(
        "GET_TO_VIRTUAL", 
        (data.search_words, table_name, str(int(data.time)))
    )
    cursor.execute("""SELECT * FROM %s""" % table_name)
    desc = cursor.description
    row = [
        dict(zip([col[0] for col in desc], row))
        for row in cursor.fetchall()
    ]
    cursor.close()

    return row
